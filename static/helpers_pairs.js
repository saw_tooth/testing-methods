document.addEventListener("DOMContentLoaded", function (event) {
    
var checkbox_ho = document.getElementById("inhome");

checkbox_ho.addEventListener( 'change', function() {
    document.getElementById('del_sel').disabled = !this.checked;
    });
    
});

function answer(id, text)
{
    let element = document.getElementById(id);
    element.innerHTML = text;
    element.classList.add('fade');
    setTimeout(() => {
        element.classList.remove('fade');
        element.innerHTML='';
        },15000);
};


/*feedback message request*/
function mail(form)
{
    var formData = {};
    cl_form = new FormData(form)
    for (var pair of cl_form.entries())
        {
            formData[pair[0]] = pair[1];
        }

    strForm = JSON.stringify(formData, null, 2)
    form.reset();
    alert(strForm);
    console.log(strForm);
    show_message('message', 'Feedback has been send');
};


function calculate(form)
{
    var formData = {};
    cl_form = new FormData(form)
    
    let locate = parseInt(cl_form.get('location'));
    let weight =  parseInt(cl_form.get('weight'));
    let inhome =  parseInt(cl_form.get('inhome_sel'));
    if (!inhome)
    {
        inhome=0;
    }
    var result = locate * weight/2 + parseInt(inhome * 4);
    answer('message', 'Cost is:' + result + '$');
};

function res(form)
{
    document.getElementById("delivery_form").reset();;    
};